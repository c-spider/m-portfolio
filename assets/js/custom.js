$('.portfolio-img').on('click', function() {
    const image = $(this).find('img').attr('src');
    $('.portfolio-fullview').addClass('show');
    $('.portfolio-fullview').find('img').attr('src', image);
    $('.portfolio-fullview').find('img').attr('link',$(this).attr('link'));
});

$('.project-link').on('click', function() {
    const link = $(this).attr("link");
    if(link=="")
        return; 
    window.open(link, "_blank");
})

$('.portfolio-fullview').on('click', function() {
    $('.portfolio-fullview').removeClass('show');
})
