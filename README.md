Hi, My name is [David Leiva](https://davidleiva.netlify.app), am a senior blockchain developer with 5 years of experience in Smart contracts and Defi development. Mostly on Ethereum, and 2 years of experience in Solana and Cardano. I'm very familiar with ERC20 token, NFT, decentralized exchange and Defi protocol development.

If you have a novel idea that you wanna develop from scratch or you wanna fork/clone anything, I will be glad to work with you.
------------------


Hi, My name is David Leiva, am a senior blockchain developer with 5 years of experience in Smart contracts and Defi development. Mostly on Ethereum, and 2 years of experience in Solana and Cardano. I'm very familiar with ERC20 token, NFT, decentralized exchange and Defi protocol development.

Blockchains
- Ethereum (Ethereum, Binance, Polygon for 5 years)
- Solana (2 year)
- Cardano (1 year)

Programming Languages
- Solidity
- Rust
- Haskell

Web Development
- MERN, MEAN